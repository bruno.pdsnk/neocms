<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Categories;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 150);
            $table->string('description', 350)->nullable();
            $table->string('icon', 150)->nullable();
            $table->unsignedInteger('father_id')->nullable();
            $table->foreign('father_id')->references('id')->on('categories');
            $table->timestamps();
        });

        $this->createCategory('Serviços', 'Cadastro de serviços oferecidos', 'fas fa-cogs');
        $this->createCategory('Produtos', 'Produtos da empresa', 'fas fa-cart');
        $this->createCategory('Institucional', 'Um pouco sobre a empresa', 'fas fa-cart');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }

    private function createCategory($name, $description, $icon, $father = null)
    {
        $i = new Categories;
        $i->name = $name;
        $i->description = $description;
        $i->icon = $icon;
        $i->father_id = $father;
        $i->save();
    }
}
