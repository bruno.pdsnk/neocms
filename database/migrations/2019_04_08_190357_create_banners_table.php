<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Banners;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('page');
            $table->integer('status')->default(1);
            $table->timestamps();
        });
        $this->createBanner('Home', 'home', 1);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }

    private function CreateBanner($name, $page, $status)
    {
        $i = new Banners;
        $i->name = $name;
        $i->page = $page;
        $i->status = $status;
        $i->save();
    }
}
